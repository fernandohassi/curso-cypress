
describe("Tickets", () => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"))

    it("fill all the text input fields", () => {
        let firstName = 'Fernando';
        let lastName = 'Hassi';

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("fernandohassi@email.com");
        cy.get("#requests").type("Soccer Player");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("select 'vip' ticket type", () => {
        cy.get("#vip").check();
    });

    it("select 'social media' checkbox", () => {
        cy.get("#social-media").check();
    });

    it("select 'friend', and 'publication', then uncheck 'friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    it("has 'TICKETBOX' headers's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });
    it("alerts on invalid email", () => {
        cy.get("#email")
            .as("email")
            .type("fernandohassiemail.com");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
            .clear()
            .type("fernandohassi@email.com");

        cy.get("#email.invalid").should("not.exist");
    });

    it("fill and reset the form", () => {
        let firstName = 'Fernando';
        let lastName = 'Hassi';
        let fullName = `${firstName} ${lastName}`;
        let ticketQuantity = '2';

        cy.get(`#first-name`).type(firstName);
        cy.get(`#last-name`).type(lastName);
        cy.get(`#email`).type(`fernandohassi@email.com`);
        cy.get(`#ticket-quantity`).select(`${ticketQuantity}`);
        cy.get(`#vip`).check();
        cy.get(`#friend`).check();
        cy.get(`#requests`).type(`Soccer`);

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy ${ticketQuantity} VIP tickets.`
        );

        cy.get("#agree").click();
        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
            .as("submitButton") //isso criar um alias que permite que seja usado com @submitButton e outras parte do código
            .should("not.be.disabled");

        cy.get("button[type='reset']").click();
        cy.get("@submitButton").should("be.disabled");
    });

    it("fills mandatory fields using support command", () => {
        const customer = {
            firstName: 'Isabela',
            lastName: 'Hassi',
            email: 'isabelahassi@example.com'
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
            .as("submitButton") //isso criar um alias que permite que seja usado com @submitButton e outras parte do código
            .should("not.be.disabled");
        cy.get("#agree").uncheck();
        cy.get("@submitButton").should("be.disabled");
    });
});

//root:ENKts4H7tXKlDJ3X6riLaewSWA6DV!bDBMmqSTmj9TSV&EXYsR

//access tokens: A8pgzMg_YY6bQSeKf1D7